import React from "react";

function App() {
  return (
    <div className="App">
      <h1 className="container" style={{ textAlign: "center"}}>My Weather App</h1>
      <WeatherBox />
    </div>
  );
}

class WeatherBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      temp: "",
      place: "",
      country: "",
      humidity: "",
      clouds: "",
      showWeather: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNewSearch = this.handleNewSearch.bind(this);
  }

  handleChange(event) {
    this.setState({ place: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();

    async function getWeatherData(place) {
      try {
        const API = `https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/weather?q=${place}&units=metric&APPID=27e2e2f65292776a80aefa087f518775`;
        const response = await fetch(API, { mode: "cors" });
        const weatherData = await response.json();
        console.log(weatherData);

        const weatherValue = document.getElementById("weather-value");
        const temperatureValue = document.getElementById("temperature-number");
        const rainValue = document.getElementById("rain-number");
        const cloudsValue = document.getElementById("clouds-number");
        const windValue = document.getElementById("wind-number");
        const iconValue = document.getElementById("weather-icon");

        weatherValue.innerHTML = weatherData.name + ", " + weatherData.sys.country;
        temperatureValue.innerHTML = weatherData.main.temp + "°";
        rainValue.innerHTML = weatherData.main.humidity + "%";
        cloudsValue.innerHTML = weatherData.clouds.all + "%";
        windValue.innerHTML = weatherData.wind.speed + "m/s";

        var iconCode = weatherData.weather[0].icon;
        var iconURL = "http://openweathermap.org/img/wn/" + iconCode + ".png";
        iconValue.src = iconURL;
      } catch(error) {
        console.log(error);
        
      }
    }

    getWeatherData(this.state.place);
    this.setState({ showWeather: true });
  }

  handleNewSearch(event) {
    event.preventDefault();
    console.log(event);
    this.setState({ showWeather: false, place: ""});
  }

  render() {
    const { showWeather } = this.state;

    return (
      <div id="weather-box" className="container">
        <form onSubmit={this.handleSubmit}>
          <label htmlFor="city-input">City</label>
          <input
            value={this.state.place + this.state.country}
            onChange={this.handleChange}
            className="u-full-width"
            type="text"
            id="city-input"
            placeholder="search city..."
          />
          <div className="row">
          <button onClick={this.handleSubmit} className="one-half column button-primary">
            Search
          </button>
          <button onClick={this.handleNewSearch} className="one-half column button-primary">
            New Search
          </button>
          </div>
          
        </form>
        <h3
          id="weather-value"
          style={{ display: showWeather ? "block" : "none", textAlign: "center" }}
        >
          WeatherTemp
        </h3>
        <div
          className="container"
          style={{ display: showWeather ? "block" : "none" }}
        >
          <div className="row">
            <div className="one-half column">
              <div id="temperature-title">
                <h4>Temperature</h4>
              </div>
              <div className="row">
                <div className="one-half column">
                  <h5 id="temperature-number" style={{textAlign: "center"}}>number</h5>
                </div>
                <div className="one-half column">
                  <img id="weather-icon" alt="weather.icon" src="" />
                </div>
              </div>
            </div>
            <div className="one-half column">
              <h4>Humidity</h4>
              <h5 id="rain-number">number</h5>
            </div>
          </div>
          <div className="row">
            <div className="one-half column">
              <h4>Clouds</h4>
              <h5 id="clouds-number">number</h5>
            </div>
            <div className="one-half column">
              <h4>Wind</h4>
              <h5 id="wind-number">number</h5>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;